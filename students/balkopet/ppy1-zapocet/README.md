This project analyses the file sharing website. Putting all read data to a series of graphs and charts.
Using:
    - Jupyter Notebook
    - reading from an SQLite database
    - reading and writing to JSON files in python
    - NumPy
    - Matplotlib

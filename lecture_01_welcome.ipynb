{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "7ed8199c",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Welcome to PPY1 lecture #1\n",
    "\n",
    "1. Grab a place, start your computer, if you can\n",
    "2. Have a look at an intro survey [bit.ly/ppy1](http://bit.ly/ppy1), which will help me fine-tune and organize the course a bit better\n",
    "3. Bookmark the repository: [gitlab.fjfi.cvut.cz/ksi/ppy1-2024](https://gitlab.fjfi.cvut.cz/ksi/ppy1-2024), open the `lecture_01.ipynb`, where you will find everything for today"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35505037",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Let me introduce first...\n",
    "\n",
    "My name is Matej Mojzeš and I will be your lecturer, I also:\n",
    "\n",
    "* Have a Ph.D. in integer optimization heuristics at the Department of Software Engineering, FNSPE CTU in Prague\n",
    "* Lead data scientists and data engineers in [SharpGrid](https://www.sharpgrid.com) in (using Python, of course!)\n",
    "* Possess years of experience in Business Intelligence, Data Warehousing, Engineering, and Data Science in both start-up and corporate environments\n",
    "\n",
    "Disclaimer: I am a big fan of Python and actively used it for many years, and surely I do NOT know everything about it, but I love learning, and I hope to learn something new together... 🙂"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63e5fe92",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# So, why bother learning more about Python?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b6eb585b",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "![image](figures/python_vs_matlab.png)\n",
    "\n",
    "Because of the ease of use, versatility, and large community which made it a popular choice for developers of all skill levels. From my point of view, currently, it is the sweet spot between prototyping and production-grade programming language.\n",
    "\n",
    "Source of the data: [trends.google.com](https://trends.google.com/trends/explore?date=all&q=%2Fm%2F05z1_,%2Fm%2F07sbkfb,%2Fm%2F0jgqg,%2Fm%2F053_x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ca69459",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# PPY1 outline\n",
    "\n",
    "1. Utilizing the Jupyter Notebook and Lab environment\n",
    "2. Object-oriented programming in Python (introduction to OOP, classes, properties, methods)\n",
    "3. Functional programming in Python (map, filter, reduce, zip, lambda expressions, ...)\n",
    "4. Introduction to NumPy and SciPy libraries (ndarray, broadcasting, vectorization)\n",
    "5. Data analysis and processing using the pandas library\n",
    "6. Data visualization using the matplotlib and seaborn libraries"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1737ce6d",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "7. Creating web applications using the Streamlit framework\n",
    "8. Data export, working with data formats such as JSON, XML, CSV, and Parquet\n",
    "9. Working with SQL databases\n",
    "10. Distributed computing using PySpark\n",
    "11. Code testing, introduction to the PyTest and Hypothesis libraries\n",
    "12. Documentation creation, conventions for writing docstrings, and typing\n",
    "13. Creating command-line interfaces (argparse/click) and APIs (Flask/FastAPI)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4bc7dce3",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "**If you feel you know all of this already, feel free to deliver your project ASAP and skip the rest of the lectures**!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39753372",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# To pass PPY1... \n",
    "\n",
    "...you need to deliver and defend your project:\n",
    "\n",
    "* it should prove that you have mastered at least 3 of the 13 areas we will cover (and your knowledge should cover the remainder, this is important for the rest of your study),\n",
    "* I would like you to choose your own topic, but I can help with the selection, or provide some options."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "59bf92f3",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "An example: a scientific project processing experimental data using pandas and numpy packages, delivered as Jupyter notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e98da7eb",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Another example: a game built using OOP, covered with PyTest unit tests, and documented using docstrings."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77d06445",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Or... a CLI tool, that will read data stored in an SQL database, and visualize them in an interactive way using a Streamlit dashboard."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f62cd05c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![image](figures/learning.jpeg)\n",
    "\n",
    "To get the most of this Python course, I strongly encourage you to present your work to your colleagues during the semester."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "742f0ca3",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "...which brings us to the question of..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f0d05986",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# What is the deadline?\n",
    "\n",
    "Ideally before the summer holidays. After, or during, is fine, too. \n",
    "\n",
    "**⚠️ ⚠️ ⚠️ But...** avoid consulting and delivering the project in the last week of the \"zkouškové období\". Based on my own statistical analysis of the usual outcome, the quality is most likely very poor. And unfortunately, I can not guarantee my availability in this period, either."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4f3217f6",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# And finally... something about Python 😉\n",
    "\n",
    "Can you run `python` or `python3` in your command prompt/terminal, and get a similar result?\n",
    "\n",
    "![image](figures/prompt_windows.jpeg)\n",
    "\n",
    "If not, head to [python.org/downloads/](https://www.python.org/downloads/), download and install the latest Python for your machine. Make sure you can get a similar result to the above (ie. you can see Python 3 running on your machine)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "73d0e60f",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "After we have Python, the next essential thing for the lecture is to have Jupyter Lab environment and an IDE.\n",
    "\n",
    "To install Jupyter Lab, just run `pip/pip3 install jupyterlab` in your console. You can run it then using `jupyter lab` command. More information on [jupyter.org/install](https://jupyter.org/install).\n",
    "\n",
    "For IDE, I recommend [Visual Studio Code](https://code.visualstudio.com), but feel free to use whatever suits your needs and preferences."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cfa7e085",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "We should be ready for the Hello World exercise!\n",
    "\n",
    "1. Start your `jupyter lab` in the console (in case you have not done it already, of course)\n",
    "2. In the UI (within web browser, which should start automatically), create a **New notebook** with Python kernel, type `print('Hello World')` in the first cell, execute it using the Run button and the result should look like this:\n",
    "\n",
    "![image](figures/hello_world.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "014febd3",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "So, what just happened?\n",
    "\n",
    "* You have executed one line of python code\n",
    "* Python (in a \"kernel\") is still running on your computer, ready to execute the code in that cell again, or any new cell you create and in the order in which you will run them\n",
    "* Results (visual outputs) of the cells will remain in the notebook after you shut down the Python kernel, you can get back to it anytime you want\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f18e7774",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Just a couple of notes:\n",
    "* To maximize the chance you will get the same output when running the notebook again, you should \"restart & run all cells\" in your notebook before saving it (but sometimes this is computationally not possible, so you just need to think in advance, to eg. not overwrite memory, delete necessary cells, etc.)\n",
    "* You have witnessed how reproducible research can be easily implemented, see a [Nature article](http://www.nature.com/news/interactive-notebooks-sharing-the-code-1.16261) for more on this topic\n",
    "* For learning purposes, we will be primarily using this interactive coding approach (rather than programming larger scripts/modules and running them all in once)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2a372cd",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let's have a look at the common tricks the Jupyter Lab can do, mainly:\n",
    "\n",
    "* Organize notebooks\n",
    "* Manage kernels\n",
    "* Work with notebooks (more at once, even the same notebooks)\n",
    "* Work with other file formats — python, markdown, data, text..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6e83875",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "You can use also our own FJFI Jupyter Hub on [jupyter.fjfi.cvut.cz](https://jupyter.fjfi.cvut.cz/). \n",
    "\n",
    "In any case, **make sure you backup (or version control, ideally) your work**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d1b44f70",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Also, you might be asking what the `pip` actually means...\n",
    "\n",
    "* It is a package manager for Python, and it stands for \"Preferred Installer Program\" or recursively \"Pip Installs Packages.\"\n",
    "* Pip is primarily used to install Python packages from the Python Package Index (PyPI) or other sources. You can install a package by running the command `pip install <package_name>` (which you probably did already)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5c366c6a",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Pip allows you to manage installed packages by providing commands to upgrade, uninstall, or list them. Eg.:\n",
    "\n",
    "```\n",
    "pip install --upgrade <package_name>\n",
    "pip uninstall <package_name>\n",
    "pip list\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "541be912",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Furthermore, pip can be helpful in some more advanced scenarios, where you need to carefully control the environment your application runs in using a virtual environment, or `venv`, managing the dependencies using a \"requirements\" file, or, for example, distribute your own packages.\n",
    "\n",
    "We will not cover pip in more detail within PPY1, but you can find very helpful resources here: https://docs.python.org/3/tutorial/venv.html."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "218b793f",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Last, but not least\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "79337ba6",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "magic = 'As you can see, this slideshow can run Python code inside the slide. This is another power of JuPYter notebooks!'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "4903fc0a",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "As you can see, this slideshow can run Python code inside the slide. This is another power of JuPYter notebooks!\n"
     ]
    }
   ],
   "source": [
    "print(magic)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3e49757",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**Inspiration/disclaimer**: Parts of this document have been created using [W3schools](https://www.w3schools.com), [Stack Overflow](https://stackoverflow.com), or [ChatGPT](https://chat.openai.com) (including a critical evaluation of the output). This is to demonstrate the power of publicly available resources. Make sure you will use them to your advantage too. Try to research what you want online, learn on your own and at your own pace, and feel free to ask me questions / discuss topics that interest you. \n",
    "\n",
    "In general, I am very happy to share my knowledge, experience, or opinions with you, rather than just repeat what is easy to find online."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
